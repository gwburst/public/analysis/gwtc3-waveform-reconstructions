#!/bin/bash

trap '{ echo "Hey, you pressed Ctrl-C.  Time to quit." ; exit 1; }' INT

GW_LIST=("S191109d" "S191127p" "S191204r" "S191215w" "S191222n" "S191230an" "S200128d" "S200129m" "S200208q" "S200209ab" "S200219ac" "S200224ca" "S200225q" "S200311bg")

if [ "$1" == '' ]; then
  echo ''
  echo 'mkcopy.sh par1'
  echo ''
  echo ' par1 (optional) available options ...'
  echo ''
  echo ' default -> all'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  exit 0
fi

# check if $1 is defined in the GW_LIST
CHECK='false'
for gwname in ${GW_LIST[@]} ; do
  if [ "$1" == $gwname ]; then CHECK='true'; fi;
done
if [ "$1" == 'all' ]; then CHECK='true'; fi;
if [ "$CHECK" == 'false' ]; then
  echo ''
  echo "Error: event name $1 doesn't exist in the GW_LIST"
  echo ''
  echo ' available events are:'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  echo ' in order to include the event in the list define from bash the CWB_GWNAME environmental variable'
  echo ''
  echo "  export CWB_GWNAME=$1 "
  echo ''
  exit 1
fi

# copy O3b cwb skymap fits files
for gwname in ${GW_LIST[@]} ; do

  # in order to mitigate sly localization bias, where is possible we have used the HLV skymap relaxing the gamma netrho constraint parameters

  idir='';

  if [ "$gwname" == 'S191127p'  ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01c_LHV_BBH_BKG_cwb_o3b_evt_run1/report/ced/gamma_n0d5';	   fi;
  # this is a IMBHB event in HL but is reconstructed in HLV only by the BBH search (doesn't exceeds the subnet threshold)
  if [ "$gwname" == 'S200209ab' ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01c_LHV_BBH_BKG_cwb_o3b_evt_run1/report/ced/gamma_n1d0';	   fi;
  if [ "$gwname" == 'S200224ca' ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01c_LHV_BBH_BKG_cwb_o3b_evt_run1/report/ced/gamma_n0d5';	   fi;
  if [ "$gwname" == 'S200311bg' ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01c_LHV_BBH_BKG_cwb_o3b_evt_run1/report/ced/gamma_n0d5';	   fi;

  if [ "$gwname" == 'S191215w'  ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01c_LHV_BBH_BKG_cwb_o3b_evt_run1/report/ced/gamma_n0d5';  fi;
  if [ "$gwname" == 'S191230an' ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01c_LHV_BBH_BKG_cwb_o3b_evt_run1/report/ced/gamma_n0d5';	   fi;
  if [ "$gwname" == 'S200208q'  ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/IMBHB/LHV/EVT/O3_K99_C01c_LHV_IMBHB_BKG_cwb_o3b_evt_run1/report/ced/gamma_n0d5';  fi;
  if [ "$gwname" == 'S200219ac' ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/IMBHB/LHV/EVT/O3_K99_C01c_LHV_IMBHB_BKG_cwb_o3b_evt_run1/report/ced/gamma_n0d5';  fi;
  # the IMBHB event S200129m is not reconstructed in HLV
  # the IMBHB event S200216br is not reconstructed in HLV

  if [ "$idir" != '' ] && ([ $1 == $gwname ] || [ $1 == 'all' ]); then 
    #mkdir -p "events/$gwname/data";
    cmd="cp $idir/$gwname/*/skyprobcc.fits events/$gwname/data/cwb_skymap_HLV.fits"
    echo $cmd; $cmd
    if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
  fi

done

exit 0

trap - INT
echo "One more time, but Ctrl-C should work again."
exit 1

