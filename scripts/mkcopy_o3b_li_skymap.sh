#!/bin/bash

trap '{ echo "Hey, you pressed Ctrl-C.  Time to quit." ; exit 1; }' INT

GW_LIST=("S191109d" "S191127p" "S191204r" "S191215w" "S191222n" "S191230an" "S200128d" "S200129m" "S200208q" "S200209ab" "S200216br" "S200219ac" "S200224ca" "S200225q" "S200311bg")

if [ "$1" == '' ]; then
  echo ''
  echo 'mkcopy.sh par1'
  echo ''
  echo ' par1 (optional) available options ...'
  echo ''
  echo ' default -> all'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  exit 0
fi

# check if $1 is defined in the GW_LIST
CHECK='false'
for gwname in ${GW_LIST[@]} ; do
  if [ "$1" == $gwname ]; then CHECK='true'; fi;
done
if [ "$1" == 'all' ]; then CHECK='true'; fi;
if [ "$CHECK" == 'false' ]; then
  echo ''
  echo "Error: event name $1 doesn't exist in the GW_LIST"
  echo ''
  echo ' available events are:'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  echo ' in order to include the event in the list define from bash the CWB_GWNAME environmental variable'
  echo ''
  echo "  export CWB_GWNAME=$1 "
  echo ''
  exit 1
fi

# copy O3b cwb skymap fits files
for gwname in ${GW_LIST[@]} ; do

  li_skymap_dir='';

  if [ "$gwname" == 'S191109d'  ]; then li_skymap_dir='/home/pe.o3/public_html/LVC/o3b-catalog/S191109a/Prod5/results/samples/Prod5_skymap.fits';	fi;
  if [ "$gwname" == 'S191127p'  ]; then li_skymap_dir='/home/pe.o3/public_html/LVC/o3b-catalog/S191127a/Prod7/results/samples/Prod7_skymap.fits';	fi;
  if [ "$gwname" == 'S191204r'  ]; then li_skymap_dir='/home/pe.o3/public_html/LVC/o3b-catalog//S191204a/Prod14/results/samples/Prod14_skymap.fits';	fi;
  #if [ "$gwname" == 'S191215w'  ]; then li_skymap_dir='/home/pe.o3/public_html/LVC/o3b-catalog/S191215a/Prod6/results/samples/Prod6_skymap.fits';	fi;
  if [ "$gwname" == 'S191215w'  ]; then li_skymap_dir='/home/charlie.hoy/projects/O3/S191215a/Prod6/webpage__with_master/samples/Prod6_skymap.fits';	fi;
  if [ "$gwname" == 'S191222n'  ]; then li_skymap_dir='/home/pe.o3/public_html/LVC/o3b-catalog/GW191222A/Prod4/results/samples/Prod4_skymap.fits';	fi;
  if [ "$gwname" == 'S191230an' ]; then li_skymap_dir='/home/pe.o3/public_html/LVC/o3b-catalog/S191230a/Prod6/results/samples/Prod6_skymap.fits';	fi;
  if [ "$gwname" == 'S200128d'  ]; then li_skymap_dir='/home/pe.o3/public_html/LVC/o3b-catalog/S200128a/Prod6/results/samples/Prod6_skymap.fits';	fi;
  if [ "$gwname" == 'S200129m'  ]; then li_skymap_dir='/home/pe.o3/public_html/LVC/o3b-catalog/S200129a/Prod5/results/samples/Prod5_skymap.fits';	fi;
  if [ "$gwname" == 'S200208q'  ]; then li_skymap_dir='/home/pe.o3/public_html/LVC/o3b-catalog/S200208a/Prod6/results/samples/Prod6_skymap.fits';	fi;
  if [ "$gwname" == 'S200209ab' ]; then li_skymap_dir='/home/pe.o3/public_html/LVC/o3b-catalog/S200209a/Prod6/results/samples/Prod6_skymap.fits';	fi;
  if [ "$gwname" == 'S200216br' ]; then li_skymap_dir='/home/pe.o3/public_html/LVC/o3b-catalog/GW200216/Prod7/results/samples/Prod7_skymap.fits';	fi;
  if [ "$gwname" == 'S200219ac' ]; then li_skymap_dir='/home/pe.o3/public_html/LVC/o3b-catalog/S200219a/Prod5/results/samples/Prod5_skymap.fits';	fi;
  if [ "$gwname" == 'S200224ca' ]; then li_skymap_dir='/home/pe.o3/public_html/LVC/o3b-catalog/S200224a/Prod5/results/samples/Prod5_skymap.fits';	fi;
  if [ "$gwname" == 'S200225q'  ]; then li_skymap_dir='/home/pe.o3/public_html/LVC/o3b-catalog/S200225a/Prod5/results/samples/Prod5_skymap.fits';	fi;
  if [ "$gwname" == 'S200311bg' ]; then li_skymap_dir='/home/pe.o3/public_html/LVC/o3b-catalog/GW200311B/Prod5/results/samples/Prod5_skymap.fits';	fi;


  net="HLV";
  if [ "$gwname" == 'S191109d' ]; then net='HL';	fi;
  if [ "$gwname" == 'S191204r' ]; then net='HL';	fi;
  if [ "$gwname" == 'S191222n' ]; then net='HL';	fi;
  if [ "$gwname" == 'S200128d' ]; then net='HL';	fi;
  if [ "$gwname" == 'S200225q' ]; then net='HL';	fi;

  approximant="IMRPhenomXPHM";

  if ([ $1 == $gwname ] || [ $1 == 'all' ]); then 
    cmd="cp $li_skymap_dir events/$gwname/data/li_skymap_$net".fits
    echo $cmd; $cmd
    if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
  fi

done

exit 0

trap - INT
echo "One more time, but Ctrl-C should work again."
exit 1

