# -*- coding: utf-8 -*-
#
# Copyright (C) 2011-2018 Leo Singer, Marek Szczepanczyk, Marco Drago, Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# colors following the palette: https://matplotlib.org/3.3.1/gallery/color/named_colors.html

from ligo.skymap.tool import ArgumentParser, FileType, SQLiteType
import matplotlib.font_manager as font_manager
import sys

def plot(files,labels,ofile,colors,pp):

    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib import rcParams
    import healpy as hp
    from ligo.skymap.io import fits
    from ligo.skymap import plot
    from ligo.skymap import postprocess

    plt.figure(figsize=(8,4))
    ax = plt.axes([0.06,0.05,0.90,0.76],projection='astro hours mollweide')
    ax.grid()

    for i in range(0,len(files)):
       f = files[i]
       skymap, metadata = fits.read_sky_map(f, nest=None)
       nside = hp.npix2nside(len(skymap))
       deg2perpix = hp.nside2pixarea(nside, degrees=True)
       cls = 100 * postprocess.find_greedy_credible_levels(skymap)
       ii = np.round(np.searchsorted(np.sort(cls), pp) * deg2perpix).astype(int)
       print('Sky area at',pp,'% probability =',ii)
       ax.contour_hpx(cls,nested=metadata['nest'], colors=colors[i], linewidths=2, levels=[pp], linestyles='-')
       ax.plot([],linestyle='-',color=colors[i], lw=2.0,  label="%s - %g%% area: %i $deg^2$"%(labels[i].replace("#"," "),pp,ii))

    plot.outline_text(ax)

    plt.legend(loc=(0.32,0.99),fontsize=12,frameon=False)
    plt.savefig(ofile)

def check(arguments,colors):
   files=[]
   labels=[]
   ofiles=[]
   mapv=90.
   for s in arguments:
    if (s.find("map=")!=-1):
      mapv=float(s.replace("map=",""))
    elif (s.find("fits")!=-1):
      files.append(s)
    elif (s.find("png")!=-1):
      ofiles.append(s)
    else:
      labels.append(s)
   if (len(files)==0):
     print("Error, no skymap file specified")
     exit()
   if (len(ofiles)>1):
     print("Error, too many outputfiles: ",len(ofiles))
     exit()
   if (len(ofiles)==0):
     print("Error, no output file specified")
     exit()
   if(len(labels)==0):
     print("no labels specified, using filenames")
     for f in files:
       labels.append(f.split("/")[-1].replace(".fits",""))
   if(len(files)!=len(labels)):
     print("Error, #files (",len(files),") is different from #labels(",len(labels),")")
     exit()
   if (len(files)>len(colors)):
     print("Error, #colors (",len(colors),") is not enough, should be bigger thandifferent from #files(",len(files),")")
   return files,labels,ofiles[0],mapv

colors=['green','orange','blue','brown']
files,labels,ofile,mapv=check(sys.argv[1:],colors)
plot(files,labels,ofile,colors,mapv)
