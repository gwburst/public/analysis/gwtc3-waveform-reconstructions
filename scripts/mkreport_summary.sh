#!/bin/bash

trap '{ echo "Hey, you pressed Ctrl-C.  Time to quit." ; exit 1; }' INT

GW_LIST=("S191109d" "S191127p" "S191204r" "S191215w" "S191222n" "S191230an" "S200128d" "S200129m" "S200208q" "S200209ab" "S200216br" "S200219ac" "S200224ca" "S200225q" "S200311bg")

if [ "$1" == '' ]; then
  echo ''
  echo 'mkreport.sh par1 par2 par3 par4 par5'
  echo ''
  echo ' par1: available options are ...'
  echo '       time/envelope/frequency/psd/white/residuals'
  echo ''
  echo ' par2: (optional): available options: true/false(default)'
  echo ''
  echo '       if true the index.html is modified in order to be used for public pages'
  echo ''
fi

if [ "$1" == '' ]; then
  echo ''
  echo ' par1: available options: time/envelope/frequency/psd/white/residuals/"skymap/set1"/skymap/set2"'
  echo ''
  exit 0
fi

if [ "$1" == '' ]; then
  echo ''
  echo ' missing second parameter: available options ...'
  echo ''
  echo ' par2: available options: all/time/envelope/frequency/psd/white/residuals/'skymap/set1'/'skymap/set2''
  echo ''
  exit 0
fi

PUBLIC_INDEX='false'
if [ "$2" != '' ]; then
  PUBLIC_INDEX=$2
fi

HOME_WWW=$(echo "$HOME_WWW" | sed "s/~/\/~/")
HOME_WWW=$(echo "$HOME_WWW" | sed "s/\//\\\\\//g")

# check if cWB is installed
$HOME_CWB/scripts/cwb_watenv.sh
if [ $? != 0 ]; then echo ''; echo 'error: cWB must be installed !!! process terminated'; echo ''; exit 1; fi

if [ "$1" == 'all' ] || [ "$1" == 'time' ]; then
  mkdir -p reports/time/all
  rm reports/time/all/*.png;
  cd reports/time/all;
  for gwname in ${GW_LIST[@]} ; do
    if [ "$gwname" == 'S200129m'  ]; then
      cmd="ln -sf ../../../events/"$gwname"/report/time/rec_signal_time_V1.png "$gwname"_rec_signal_time_V1.png"
    else
      cmd="ln -sf ../../../events/"$gwname"/report/time/rec_signal_time_L1.png "$gwname"_rec_signal_time_L1.png"
    fi
    echo $cmd; $cmd
    cmd="ln -sf ../../../events/"$gwname"/report/time/rec_signal_time_H1.png "$gwname"_rec_signal_time_H1.png"
    echo $cmd; $cmd
  done
  cd ..; $HOME_CWB/scripts/cwb_mkhtml.csh all '--multi true --title Time#Domain#Waveforms --subtitle (cWB#max-likelihood)#vs#(90%#Confidence#Interval:#cWB#Reconstructed#LALInference#Waveforms)'

  sed -i 's/<table>/<table align="center">/'   all/png_html_index/index.html
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  all/png_html_index/index.html
  cp ../../html/time/index.html .
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  index.html
  if [ $PUBLIC_INDEX == 'true' ]; then
    # the index.html is modified in order to be used for public pages
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/\.\.\/html\//g' all/png_html_index/index.html
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/html\//g' index.html
  fi

  cd ../..
fi

if [ "$1" == 'all' ] || [ "$1" == 'envelope' ]; then
  mkdir -p reports/envelope/all
  rm reports/envelope/all/*.png;
  cd reports/envelope/all;
  for gwname in ${GW_LIST[@]} ; do
    if [ "$gwname" == 'S200129m'  ]; then
      cmd="ln -sf ../../../events/"$gwname"/report/envelope/rec_signal_envelope_V1.png "$gwname"_rec_signal_envelope_V1.png"
    else
      cmd="ln -sf ../../../events/"$gwname"/report/envelope/rec_signal_envelope_L1.png "$gwname"_rec_signal_envelope_L1.png"
    fi
    echo $cmd; $cmd
    cmd="ln -sf ../../../events/"$gwname"/report/envelope/rec_signal_envelope_H1.png "$gwname"_rec_signal_envelope_H1.png"
    echo $cmd; $cmd
  done
  cd ..; $HOME_CWB/scripts/cwb_mkhtml.csh all '--multi true --title Time#Domain#Waveforms#(envelope) --subtitle (cWB#max-likelihood)#vs#(90%#Confidence#Interval:#cWB#Reconstructed#LALInference#Waveforms)'

  sed -i 's/<table>/<table align="center">/'   all/png_html_index/index.html
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  all/png_html_index/index.html
  cp ../../html/envelope/index.html .
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  index.html
  if [ $PUBLIC_INDEX == 'true' ]; then
    # the index.html is modified in order to be used for public pages
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/\.\.\/html\//g' all/png_html_index/index.html
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/html\//g' index.html
  fi

  cd ../..
fi

if [ "$1" == 'all' ] || [ "$1" == 'frequency' ]; then
  mkdir -p reports/frequency/all
  rm reports/frequency/all/*.png;
  cd reports/frequency/all;
  for gwname in ${GW_LIST[@]} ; do
    if [ "$gwname" == 'S200129m'  ]; then
      cmd="ln -sf ../../../events/"$gwname"/report/frequency/rec_signal_frequency_V1.png "$gwname"_rec_signal_frequency_V1.png"
    else 
      cmd="ln -sf ../../../events/"$gwname"/report/frequency/rec_signal_frequency_L1.png "$gwname"_rec_signal_frequency_L1.png"
    fi
    echo $cmd; $cmd
    cmd="ln -sf ../../../events/"$gwname"/report/frequency/rec_signal_frequency_H1.png "$gwname"_rec_signal_frequency_H1.png"
    echo $cmd; $cmd
  done
  cd ..; $HOME_CWB/scripts/cwb_mkhtml.csh all '--multi true --title Frequency#Domain#Waveforms --subtitle (cWB#max-likelihood)#vs#(90%#Confidence#Interval:#cWB#Reconstructed#LALInference#Waveforms)'

  sed -i 's/<table>/<table align="center">/'   all/png_html_index/index.html
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  all/png_html_index/index.html
  cp ../../html/frequency/index.html .
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  index.html
  if [ $PUBLIC_INDEX == 'true' ]; then
    # the index.html is modified in order to be used for public pages
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/\.\.\/html\//g' all/png_html_index/index.html
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/html\//g' index.html
  fi

  cd ../..
fi
  
if [ "$1" == 'all' ] || [ "$1" == 'psd' ]; then
  mkdir -p reports/psd/all
  rm reports/psd/all/*.png;
  cd reports/psd/all;
  for gwname in ${GW_LIST[@]} ; do
    if [ "$gwname" == 'S200129m'  ]; then
      cmd="ln -sf ../../../events/"$gwname"/report/psd/H1_V1_psd.png "$gwname"_psd_H1_V1.png"
    else
      cmd="ln -sf ../../../events/"$gwname"/report/psd/H1_L1_psd.png "$gwname"_psd_H1_L1.png"
    fi
    echo $cmd; $cmd
  done
  cd ..; $HOME_CWB/scripts/cwb_mkhtml.csh all '--multi true --title Strain#Sensitivities --subtitle O3b#Events#Found#by#cWB'

  sed -i 's/<table>/<table align="center">/'   all/png_html_index/index.html
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  all/png_html_index/index.html
  cp ../../html/psd/index.html .
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  index.html
  if [ $PUBLIC_INDEX == 'true' ]; then
    # the index.html is modified in order to be used for public pages
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/\.\.\/html\//g' all/png_html_index/index.html
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/html\//g' index.html
  fi

  cd ../..
fi

if [ "$1" == 'all' ] || [ "$1" == 'white' ]; then
  mkdir -p reports/white/all
  rm reports/white/all/*.png;
  cd reports/white/all;
  for gwname in ${GW_LIST[@]} ; do
    if [ "$gwname" == 'S200129m'  ]; then
      cmd="ln -sf ../../../events/"$gwname"/report/white/white_time_V1.png "$gwname"_white_time_V1.png"
    else
      cmd="ln -sf ../../../events/"$gwname"/report/white/white_time_L1.png "$gwname"_white_time_L1.png"
    fi
    echo $cmd; $cmd
    cmd="ln -sf ../../../events/"$gwname"/report/white/white_time_H1.png "$gwname"_white_time_H1.png"
    echo $cmd; $cmd
  done
  cd ..; $HOME_CWB/scripts/cwb_mkhtml.csh all '--multi true --title Time#Domain#Waveforms --subtitle (cWB#max-likelihood)#vs#(LALInference#max-likelihood)#vs#(whitened#data#16:512#Hz)'

  sed -i 's/<table>/<table align="center">/'   all/png_html_index/index.html
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  all/png_html_index/index.html
  cp ../../html/white/index.html .
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  index.html
  if [ $PUBLIC_INDEX == 'true' ]; then
    # the index.html is modified in order to be used for public pages
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/\.\.\/html\//g' all/png_html_index/index.html
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/html\//g' index.html
  fi

  cd ../..
fi

if [ "$1" == 'all' ] || [ "$1" == 'residuals' ]; then
  mkdir -p reports/residuals/all
  rm reports/residuals/all/*.png;
  cd reports/residuals/all;
  for gwname in ${GW_LIST[@]} ; do
    if [ "$gwname" == 'S200129m'  ]; then
      cmd="ln -sf ../../../events/"$gwname"/report/residuals/residuals_time_V1.png "$gwname"_residuals_time_V1.png"
    else
      cmd="ln -sf ../../../events/"$gwname"/report/residuals/residuals_time_L1.png "$gwname"_residuals_time_L1.png"
    fi
    echo $cmd; $cmd
    cmd="ln -sf ../../../events/"$gwname"/report/residuals/residuals_time_H1.png "$gwname"_residuals_time_H1.png"
    echo $cmd; $cmd
  done
  cd ..; $HOME_CWB/scripts/cwb_mkhtml.csh all '--multi true --title Time#Domain#Residuals --subtitle residuals#(LALInference-cWB)#vs#residuals#(data-cWB)'

  sed -i 's/<table>/<table align="center">/'   all/png_html_index/index.html
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  all/png_html_index/index.html
  cp ../../html/residuals/index.html .
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  index.html
  if [ $PUBLIC_INDEX == 'true' ]; then
    # the index.html is modified in order to be used for public pages
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/\.\.\/html\//g' all/png_html_index/index.html
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/html\//g' index.html
  fi

  cd ../..
fi

if [ "$1" == 'skymap/set1' ] || [ "$1" == 'skymap/set2' ]; then
  mkdir -p reports/$1/all
  rm reports/$1/all/*.png
  cd reports/$1/all;
  for gwname in ${GW_LIST[@]} ; do
    li_net="HLV"
    cwb_net="HL"
    if [ "$gwname" == "S191109d" ]; then
       li_net="HL"
    fi
    if [ "$gwname" == "S191204r" ]; then
       li_net="HL"
    fi
    if [ "$gwname" == "S191222n" ]; then
       li_net="HL"
    fi
    if [ "$gwname" == "S200128d" ]; then
       li_net="HL"
    fi
    if [ "$gwname" == "S200225q" ]; then
       li_net="HL"
    fi

    # make cwb HLV skymap report 
    if [ "$li_net" == 'HLV' ] && [ "$1" == 'skymap/set2' ]; then
      cwb_net="HLV"
    fi 

    if [ "$gwname" == "S200129m" ]; then
       cwb_net="HV"
    fi
    if [ "$gwname" == "S200216br" ]; then
       cwb_net="HL"
    fi
 
    cmd="ln -sf ../../../../events/$gwname/report/skymap/cwb_li_skymap90_"$cwb_net"_"$li_net".png "$gwname"_cwb_li_skymap90_"$cwb_net"_"$li_net".png"
    echo $cmd; $cmd
  done
  cd ../../..; $HOME_CWB/scripts/cwb_mkhtml.csh $1/all '--multi true --title Skymaps --subtitle O3b#Events#Found#by#cWB#(#cWB#vs#LALInference)'

  sed -i 's/<table>/<table align="center">/'   $1/all/png_html_index/index.html
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g' $1/all/png_html_index/index.html
  cp ../html/$1/index.html $1/index.html
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  $1/index.html
  if [ $PUBLIC_INDEX == 'true' ]; then
    # the index.html is modified in order to be used for public pages
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/\.\.\/\.\.\/html\//g' $1/all/png_html_index/index.html
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/html\//g' $1/index.html
  fi

  fig_id=1

  sed -i 's/<h2>'$fig_id'<\/h2>/<h2>S191109d<\/h2>/'   $1/all/png_html_index/index.html;	let "fig_id++"
  sed -i 's/<h2>'$fig_id'<\/h2>/<h2>S191127p<\/h2>/'   $1/all/png_html_index/index.html;	let "fig_id++"
  sed -i 's/<h2>'$fig_id'<\/h2>/<h2>S191204r<\/h2>/'   $1/all/png_html_index/index.html;	let "fig_id++"
  sed -i 's/<h2>'$fig_id'<\/h2>/<h2>S191215w<\/h2>/'   $1/all/png_html_index/index.html;	let "fig_id++"
  sed -i 's/<h2>'$fig_id'<\/h2>/<h2>S191222n<\/h2>/'   $1/all/png_html_index/index.html;	let "fig_id++"
  sed -i 's/<h2>'$fig_id'<\/h2>/<h2>S191230an<\/h2>/'  $1/all/png_html_index/index.html;	let "fig_id++"
  sed -i 's/<h2>'$fig_id'<\/h2>/<h2>S200128d<\/h2>/'   $1/all/png_html_index/index.html;	let "fig_id++"
  sed -i 's/<h2>'$fig_id'<\/h2>/<h2>S200129m<\/h2>/'   $1/all/png_html_index/index.html;	let "fig_id++"
  sed -i 's/<h2>'$fig_id'<\/h2>/<h2>S200208q<\/h2>/'   $1/all/png_html_index/index.html;	let "fig_id++"
  sed -i 's/<h2>'$fig_id'<\/h2>/<h2>S200209ab<\/h2>/' $1/all/png_html_index/index.html;		let "fig_id++"
  sed -i 's/<h2>'$fig_id'<\/h2>/<h2>S200216br<\/h2>/' $1/all/png_html_index/index.html;		let "fig_id++"
  sed -i 's/<h2>'$fig_id'<\/h2>/<h2>S200219ac<\/h2>/' $1/all/png_html_index/index.html;		let "fig_id++"
  sed -i 's/<h2>'$fig_id'<\/h2>/<h2>S200224ca<\/h2>/' $1/all/png_html_index/index.html;		let "fig_id++"
  sed -i 's/<h2>'$fig_id'<\/h2>/<h2>S200225q<\/h2>/'  $1/all/png_html_index/index.html;		let "fig_id++"
  sed -i 's/<h2>'$fig_id'<\/h2>/<h2>S200311bg<\/h2>/' $1/all/png_html_index/index.html;		let "fig_id++"

  cd ../..
fi

exit 0

trap - INT
echo "One more time, but Ctrl-C should work again."
exit 1

