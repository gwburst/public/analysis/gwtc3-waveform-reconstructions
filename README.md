# cWB: O3b Waveform Reconstructions

Coherent WaveBurst (cWB) is a detection and waveform reconstruction pipeline used by the LIGO/Virgo Collaboration. Coherent WaveBurst makes minimal assumptions on signal shape and utilizes the coherence of the wavelet decompositions of the signals from different detectors to detect gravitational waves. Much more on cWB can be found in the [online documentation](https://gwburst.gitlab.io/).

This repository contains a summary of the coherent WaveBurst O3b waveform reconstruction results. Part of this material was used within the [O3b Catalog paper](https://dcc.ligo.org/P2000061/public).

**[This figure](https://git.ligo.org/cWB/analysis/o3b-waveform-reconstructions/-/blob/master/events/S191109d/report/time/rec_signal_time_L1.png)** is an example of time-domain representation of a reconstruction, where the solid line represents the cWB maximum likelihood reconstruction and the grey bands delimit the 90% (pointwise) confidence interval with symmetric tails. Similar representations are given for the time-domain envelope as in **[this figure](https://git.ligo.org/cWB/analysis/o3b-waveform-reconstructions/-/blob/master/events/S191109d/report/envelope/rec_signal_envelope_L1.png)**, and for the frequency-domain representation (PSD), as in **[this figure](https://git.ligo.org/cWB/analysis/o3b-waveform-reconstructions/-/blob/master/events/S191109d/report/frequency/rec_signal_frequency_L1.png)**. For more details on the reconstruction, see below.


## Description of the repository contents

- The `events` directory contains data and reconstructions for each event.
- the `data` subdirectory contains human-readable data files in column-wise format:
    - **maxl_li_signal_time** files: LALInference Maximum Likelihood h in the time domain  [GPS time, maxL h(t)]

    - **rec_signal_time** files: cWB Maximum Likelihood reconstruction of the whitened strain _h(t)_ in the time domain  [GPS time, amplitude of cWB reconstruction, median amplitude of cWB reconstruction of off-source injections, lower 50% boundary of cWB reconstruction of off-source injections, lower 90% boundary of cWB reconstruction of off-source injections, upper 50% boundary of cWB reconstruction of off-source injections, upper 90% boundary of cWB reconstruction of off-source injections]

    - **rec_signal_envelope** files: time envelope of cWB reconstruction of the whitened strain _h(t)_  [GPS time, amplitude of the time envelope, median amplitude of the time envelope of off-source injections, lower 50% boundary of the time envelope of off-source injections, lower 90% boundary of the time envelope of off-source injections, upper 50% boundary of the time envelope of off-source injections, upper 90% boundary of the time envelope of off-source injections]

    - **rec_signal_frequency** files: cWB Maximum Likelihood reconstruction of _h(t)_ in the frequency domain  [GPS time, magnitude of cWB reconstruction, median magnitude of cWB reconstruction of off-source injections, lower 50% boundary of cWB reconstruction of off-source injections, lower 90% boundary of cWB reconstruction of off-source injections, upper 50% boundary of cWB reconstruction of off-source injections, upper 90% boundary of cWB reconstruction of off-source injections]

    - **psd** files: Hanford & Livingston power spectral density as estimated by cWB around the event GPS time  [frequency (Hz), power spectral density (1/Hz)]

    - **whitened data** files: single-detector data whitened by cWB  [GPS time, whitened data]

    - **cWB skymaps** files: fits files

- the `report` subdirectory contains .png time-domain plots of the reconstructed signals (whole time range + zoomed version). Just as in the O3b Catalog (i.e. GWTC-2), the reconstructions of the individual events are referred to as "onsource". Here and in the Catalog, we also use "offsource data" that are obtained by adding waveforms from the Bayesian parameter estimation analysis to data near, but not including, each event. We repeated cWB analysis on these offsource data thousands of times in order to populate our empirical estimates of the  distributions of the reconstruction parameters. **The LALInference model used for comparisons is IMRPhenomXPHM.**

- the `macros` directory contains C macros used to get data files and produce plots

- the `reports` directory is just a summary of the files in the individual directories (files can be accessed from the individual events subdirectories)

- the `scripts` directory contains bash scripts used to get data files and produce plots

------------------------------------------

## More details on

- [Installation](./Install.md)
- [How to produce html pages](./Reports.md)
- [Examples](./Examples.md)
