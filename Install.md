# Installation

## Cloning the O3b-Waveform-Reconstructions repository

All data files are stored via git LFS

*If you already have git LFS installed please read this section to ensure git
LFS is configured as required*

Download and install [Git LFS](https://git-lfs.github.com/) for your system.
You need to run the following command to setup the command line extension and you only have to do this once.

```bash
git lfs install --skip-smudge
```

The `--skip-smudge` option ensures that no files under LFS control are
downloaded automatically when cloning the repository or checking out new
commits containing LFS controlled files.[**Global automatic downloads with local manual downloads**](#global-automatic-downloads-with-local-manual-downloads).

We are now ready to clone the repository:

```bash
git clone git@git.ligo.org:cWB/analysis/o3b-waveform-reconstructions.git
```

### Pulling git lfs files

To download a specific file run the following command:

```bash
git lfs pull -I <file-path>
```

This downloads a specific file identified by `<file-path>` or you can use a global pattern to download the whole directory:

```bash
git lfs pull -I "events/*/data/*"
```

---

## Requirements and setup

- The production of plots and html report pages requires the installation of the [cWB library](https://gwburst.gitlab.io/documentation/latest/html/install.html)

- For skymap plots it requires the [ligo.skymap python package](https://lscsoft.docs.ligo.org/ligo.skymap/). To install the package execute the following commands:

```bash
python3 -m venv ~/virtualenv/ligo.skymap
source ~/virtualenv/ligo.skymap/bin/activate
pip3 install ligo.skymap
```

Next, to setup the environment source `setup.sh`:

```bash
source setup.sh
```
