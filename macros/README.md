# ROOT Macro #

The following macros are used to import data, generate plots and html report pages  

* CopyEventData.C (used to import event time/envelope/frequency/psd/skymap/white/max_li data files)
* DrawEventPSD.C (used to produce PSD plot)
* DrawEventWaveforms.C (used to produce time/envelope/frequency plots)

