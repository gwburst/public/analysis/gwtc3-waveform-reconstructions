# Examples

- time plot for cWB vs LALInference 90% S191109d

```bash
     make PLOT  GW=S191109d TYPE=time
```

- html page for cWB vs LALInference 90% S191109d time plots

```bash
     make HTML  GW=S191109d TYPE=time
```

- remove html page for cWB vs LALInference 90% S191109d time plots

```bash
     make CLEAN  GW=S191109d TYPE=time
```

- make skymap plots for S191109d in batch mode

```bash
     make PLOT  GW=S191109d TYPE=time BATCH=true
```

- make skymap plots for all events in batch mode (the plots are produced in parallel)

```bash
     make PLOT  GW=all TYPE=time BATCH=true
```

- make all plots and html pages for S191109d + final event html page

```bash
     make PLOT  GW=S191109d TYPE=time
     make PLOT  GW=S191109d TYPE=envelope
     make PLOT  GW=S191109d TYPE=frequency
     make PLOT  GW=S191109d TYPE=white
     make PLOT  GW=S191109d TYPE=residuals
     make PLOT  GW=S191109d TYPE=skymap
     make PLOT  GW=S191109d TYPE=psd
```

- plot time with plot options (without white spaces)

```bash
     make PLOT  GW=S191109d TYPE=time OPTS="'{"\"GW190408_181802"\",1238782700.0,0.00,0.35,-4.0,4.0,"\"down-left"\",4}'"

     make HTML  GW=S191109d TYPE=time
     make HTML  GW=S191109d TYPE=envelope
     make HTML  GW=S191109d TYPE=frequency
     make HTML  GW=S191109d TYPE=white
     make HTML  GW=S191109d TYPE=residuals
     make HTML  GW=S191109d TYPE=skymap
     make HTML  GW=S191109d TYPE=psd
```

- final event html page: OPTS is the sub-title (optional)

```bash
     make HTML  GW=S191109d TYPE=event OPTS="'( LALInference approximant = NRSur7dq4 )'"
```

- make time plots and html pages for all events + the final summary html page:

```bash
     make PLOT  GW=all TYPE=time
     make HTML  GW=all TYPE=time
     make HTML  TYPE=summary_time
```

- remove all html event pages + summary time html page

```bash
     make CLEAN  GW=all TYPE=time
     make CLEAN  TYPE=summary_time
```

- make all types summary html pages for all events:

```bash
     make HTML  TYPE=summary_all
```

- make all types html pages for all events:

```bash
     make HTML  GW=all TYPE=all
```

- make all types plots for all events:

```bash
     make PLOT  TYPE=all
```

- O3b event data files are stored in the repository directories events/GW_NAME/data
   The O3b event data files have been copied from directories defined in the configuration file config/Makefile.cwb_pereport_config
   The commands used to load the S191109d data files into events/S191109d/data are:

```bash
     make COPY  GW=S191109d TYPE=time
     make COPY  GW=S191109d TYPE=envelope
     make COPY  GW=S191109d TYPE=frequency
     make COPY  GW=S191109d TYPE=white
     make COPY  GW=S191109d TYPE=skymap
     make COPY  GW=S191109d TYPE=psd
```

- In order to define a new event named "USER_GWNAME" not included in the O3b event list we neet to setup from bash the CWB_GWNAME environmental variable

```bash
   export CWB_GWNAME="USER_GWNAME"
```
