# Html report report pages

The repository contains data and plots files. The following procedure shows how to generate the html full report pages:

```bash
  # the following command creates the time/envelope/frequency/white/residuals/psd/skymap html report pages for each event included in the events directory
  # after the init do:
  make HTML GW=all TYPE=all
  # be patient, it takes a while
  # report pages are created in event/*/report/*/png_html_index

  # the following command creates the event html page for all events:
  make HTML  GW=all TYPE=event
  # these html page points to the time/envelope/frequency/white/residuals/psd/skymap html report pages
  # event report pages are created in event/*/report/*/all/

  # last, we create the summary pages which points to all event html pages:
  make HTML  TYPE=summary_all
  # summary report pages are created in ls reports/*/all

  # in order to generate html for public gl-pages add to the previous commands the option PUBLIC=true

  # The following home html page is a summary of all O3b reports. In order to visualize the contents use an html browser and do:
  www-browser reports/home/index.html  
```

---

## Html report pages for a user defined events

The repository contains 14 cWB O3b BBH/IMBHB events reported in the [GWTC-3 catalog paper](https://dcc.ligo.org/P2000061/public).

- S191109d S191127p S191204r S191215w S191222n S191230an S200128d S200129m S200208q S200209ab
- S200219ac S200224ca S200225q S200311bg

It is possible to use the procedures developed for GWTC-3 also for events not included in the cWB O3b BBH/IMBHB list.
Consider for example a candidate event named SYYMMDDaz and follow the procedure below (bash shell):

```bash
  mkdir SYYMMDDaz
  cd SYYMMDDaz

  # cWB and ligo.skymap must be installed
  source PATH_TO_O3b_WAVEFORM_RECONSTRUCTION_REPOSITORY/setup.sh

  # define the CWB_GWNAME variable
  export CWB_GWNAME="SYYMMDDaz"

  mkdir config
  # create in config a configuration file which contains the directories used for the cWB posteriors samples analysis
  # Makefile.cwb_pereport_config
  # see example in the repositoty: PATH_TO_O3b_WAVEFORM_RECONSTRUCTION_REPOSITORY/config/Makefile.cwb_pereport_config

  # when we are outside the PATH_TO_O3b_WAVEFORM_RECONSTRUCTION_REPOSITORY path we must use xmake instead of make
  # In the following we assume that all the cWB posteriors samples analysis have been done.

  # copy data
  xmake COPY  GW=SYYMMDDaz TYPE=time
  xmake COPY  GW=SYYMMDDaz TYPE=envelope  
  xmake COPY  GW=SYYMMDDaz TYPE=frequency
  xmake COPY  GW=SYYMMDDaz TYPE=psd
  xmake COPY  GW=SYYMMDDaz TYPE=white
  xmake COPY  GW=SYYMMDDaz TYPE=skymap

  # create plots
  xmake PLOT  GW=SYYMMDDaz TYPE=time      OPTS="'{"\"GWYYMMDD"\",1234567890.0,0.65,1.00,-6.0,6.0,"\"up-left"\",4}'"
  xmake PLOT  GW=SYYMMDDaz TYPE=envelope  OPTS="'{"\"GWYYMMDD"\",1234567890.0,0.65,1.00,-6.0,6.0,"\"up-left"\",4}'"
  xmake PLOT  GW=SYYMMDDaz TYPE=frequency OPTS="'{"\"GWYYMMDD"\",1234567890.0,0.65,1.00,-6.0,6.0,"\"up-left"\",4}'"
  xmake PLOT  GW=SYYMMDDaz TYPE=psd
  xmake PLOT  GW=SYYMMDDaz TYPE=white     OPTS="'{"\"GWYYMMDD"\",1234567890.0,0.65,1.00,-6.0,6.0,"\"down-left"\",4}'"
  xmake PLOT  GW=SYYMMDDaz TYPE=residuals OPTS="'{"\"GWYYMMDD"\",1234567890.0,0.65,1.00,-6.0,6.0,"\"down-left"\",4}'"
  xmake PLOT  GW=SYYMMDDaz TYPE=skymap

  # where OPTS are the plot user input parameters
  #
  #  format: {"name",offset,min,max,inf,sup,"leg",lwidth}    # white spaces are not allowed
  #
  #    name          // gw name: Ex: S191109d
  #    offset        // GPS offset (GPS time)
  #    min           // begin of the plot time wrt offset
  #    max           // end of the plot time wrt offset
  #    inf           // min of y axis (sec)
  #    sup           // max of y axis (sec)
  #    leg           // legend position (down-left, down-right, up-left, up-right)
  #    lwidth        // line width

  # create html report pages
  xmake HTML  GW=SYYMMDDaz TYPE=time
  xmake HTML  GW=SYYMMDDaz TYPE=envelope  
  xmake HTML  GW=SYYMMDDaz TYPE=frequency
  xmake HTML  GW=SYYMMDDaz TYPE=psd
  xmake HTML  GW=SYYMMDDaz TYPE=white
  xmake HTML  GW=SYYMMDDaz TYPE=residuals
  xmake HTML  GW=SYYMMDDaz TYPE=skymap

  # create event html report page (OPTS is the sub title)
  xmake HTML  GW=SYYMMDDaz TYPE=event OPTS="'( LALInference model = IMRPhenomXPHM )'"
```

---

## How to public reports

```bash
  export HOME_DOC="online cWB manual local git repository"
  export HOME_GWTC3="GWTC-3 cWB waveform reconstructions local git repository"

  cd $HOME_DOC
  source setup.sh

  make HTML GW=all TYPE=all  PUBLIC=true
  make HTML GW=all TYPE=event  PUBLIC=true
  make HTML TYPE=summary_all  PUBLIC=true

  cd $HOME_DOC/_gl-pages
  git rm -rf latest/gwtc-3
  git commit -a -m "documentation update"
  git push

  mkdir -p latest/gwtc-3
  cp -r $HOME_GWTC3/events latest/gwtc-3/.
  rm -rf latest/gwtc-3/events/*/data
  cp -r $HOME_GWTC3/html latest/gwtc-3/.
  cp -r $HOME_GWTC3/reports latest/gwtc-3/.

  git add --all
  git commit -a -m "documentation update"
  git push
```

[More examples](./Examples.md)
